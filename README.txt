

Paranoid Form Validator can be used to prevent security attacks (cross-site scripting, SQL injection) carried out by submitting forms with malicious data in some cases.

Usually these attempts would not be a real threat if the module developers properly verified every input data. But rarely, errare humanum est, developers make mistake and leave security holes in the web page (just take a look at Drupal security advisories).
This module is for you, if

* your site does not need formatted user input, every data entered is simple plain text, so you can afford getting rid of rich input, and
* you are developing a module and you feel insecure if you really put check_plain everywhere,
* or you use modules which you are not sure the quality of.

Usage. This module can reject any submitted form if it finds suspicious data posted. You can control which forms must be checked or which forms are trusted and no need to check. Note that data posted by users with the 'submit unchecked form' permission are not checked. Data posted from the form paranoidvalidator_admin_settings (the admin page of the module) is never checked either.

When malicious input found and considered as a possible break-in attempt, an event is triggered so you can assign actions to this trigger. Two sample actions are already present: one to log, one to display the incident. See the Triggers page for more info.

Important notes:

* This module is not enough to protect an insecure site. It only prevents users to enter malicious data in the future. If your database is already tainted, or your data came elsewhere (e.g. another application), this module does not help you.
* Generally it is not the best attitude to filter input data instead of carefully handling data inside the program code. But it is clear that if there is a bug in a module (which you do not indend to fix yourself), this decreases the possibility that a security whole can be exploited. So this module is paranoid because it assumes that some module is buggy.
* This module conflicts with any sophisticated input filter methods, it supports only the simplest case where no rich input allowed at all.

Development info:

If you want to interact with paranoidvalidator from your module, you have two option: actions or hook_paranoidvalidator.
Sample actions to trigger are already present: one to log, one to display the incident:

    /**
     * Sample handler of paranoidvalidator's incident.
     *
     * @param $post
     *   String. Tainted data posted, normally $_POST.
     * @param $key
     *   Array. The form key where incident occured.
     * @param $title
     *   String or null. The title of the form element.
     *
     */
    function _paranoidvalidator_wd_action($post, $key, $title) {
      watchdog('paranoidvalidator', 'Possible break-in attempt: %f[%k (%t)]=%v',
        array(
          // check_plain is not necessary here
          '%f' => ($post['form_id']),
          '%k' => ($key),
          '%t' => ($title),
          '%v' => ($post[$key]),
        ), WATCHDOG_ERROR);
    }

    /**
     * Sample handler of paranoidvalidator's incident.
     *
     * @see hook_paranoidvalidator.
     */
    function _paranoidvalidator_msg_action($post, $key, $title) {
      drupal_set_message(
        t('Possible break-in attempt. This incident has been logged.'),
	'error');
    }

Note that paranoidvalidator does not depend explicitly on trigger module so you might want to enable it.
Other choice is implementing the hook_paranoidvalidator() which is invoked the just before the triggers.

    /**
     * Implementation of hook_paranoidvalidator().
     *
     * This function is never called, only is a demonstration of working.
     * To make this work, rename this function to MYMODULE_paranoidvalidator.
     *
     * @param $post
     *   String. Tainted data posted, normally $_POST.
     * @param $key
     *   Array. The form key where incident occured.
     * @param $title
     *   String or null. The title of the form element.
     */
    function hook_paranoidvalidator($post, $key, $title) {
      // E.g.:
      _paranoidvalidator_wd_action($post, $key, $title);
    }
