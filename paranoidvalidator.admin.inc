<?php

// Copyright © 2009 Richard Kapolnai.
// License: GPLv2

/**
 * @file
 * Administrative page callbacks for the Paranoid Form Validator module.
 */


/**
 * paranoidvalidator module settings form.
 */
function paranoidvalidator_admin_settings() {
  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filtering forms to apply paranoid check'),
    '#description' => t('Here you can control which forms are to be checked for malicious user input. You can filter forms according to form_id and decide whether check filtered forms or not.'),
  );
  $form['filter']['paranoidvalidator_action'] = array(
    '#type' => 'radios',
    '#title' => t('Default action to do on filtered forms'),
    '#options' => array(
      'skip' => t('Trust posted data, do not check'),
      'verify' => t('Do not trust, check against malicious input'),
    ),
    '#default_value' => _paranoidvalidator_variable_get('paranoidvalidator_action'),
  );
  $form['filter']['paranoidvalidator_form_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Pattern to filter forms'),
    '#default_value' => _paranoidvalidator_variable_get('paranoidvalidator_form_pattern'),
    '#description' => t('Use php regular expression syntax to filter forms. Form ids matching this pattern will be selected for action.'),
  );
  $form['adv'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#description' => '<p>' . t('Here you can control what should be considered unsafe.')
      . t(' Modifying this setting is <b>strongly discouraged</b> unless you know what you are doing.')
      . '</p><p>'
      . t('You may not want to allow characters such as  <b>&lt; &gt;</b>  \' ;'),
  );
  $form['adv']['paranoidvalidator_d_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Pattern to consider data safe'),
    '#default_value' => _paranoidvalidator_variable_get('paranoidvalidator_d_pattern'),
    '#description' => t('Use php regular expression syntax. Final expression is built from $e: <tt>"/[^$e]+/u"</tt>.'),
  );
  return system_settings_form($form);
}
